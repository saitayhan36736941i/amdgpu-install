#############
Prerequisites
#############

.. contents:: Table of Contents

----------

Downloading the Installer Package
**********************************

The AMDGPU stack is delivered as an installer package. Prior to installation,
you must first obtain the installer package from your AMD Customer Engagement
Representative, from the
`AMD web site <https://support.amd.com/en-us/download>`_, or from
`repo.radeon.com <http://repo.radeon.com/amdgpu-install/latest>`_.

Download the installer package to the system (for example, to ``~/Downloads``),
and install it using the package manager, which will make the installation
script available on your system.

--------

Installing the Installer Package
*********************************

In the following commands, replace ``amdgpu-install-VERSION.{deb,rpm}`` with
the actual file name of the downloaded installer package:

.. code-block:: bash

 $ cd ~/Downloads
 # For Ubuntu:
 $ sudo apt-get install ./amdgpu-install-VERSION.deb
 $ sudo apt-get update
 # For RHEL:
 $ sudo yum install ./amdgpu-install-VERSION.rpm
 # For SLE:
 $ sudo zypper install ./amdgpu-install-VERSION.rpm

.. note:: Installing "amdgpu-install" will add the installer and AMD related
   repositories to your system. The repositories will be accessible to the
   system package manager, which is in turn used by the installer.


---------

Configuring Access to the Distribution Repository (RHEL and SLE only)
**********************************************************************

AMDGPU stack depends on packages provided by the Linux distribution vendors.

The AMDGPU stack requires access to specific RPMs from Red Hat Enterprise Linux
(RHEL) or SUSE Linux Enterprise (SLE) installation media for the purpose of
dependency resolution. You must ensure **one** of the following:

- Have a valid subscription and be connected to Internet during installation.
- Mount an installation media (for example, DVD, USB key or ISO file). Media
  mounting instructions for **Red Hat** systems are provided at
  https://access.redhat.com/solutions/1355683. For **SLE**, use YaST to add the
  installation media as a new repository, see `the SUSE documentation
  <https://documentation.suse.com/sles/15-SP1/html/SLES-all/cha-deployment-instserver.html#sec-remote-installation-iso>`_
  for details.

.. note:: It is assumed most customers have a subscription with access to
      online repositories or are using a Linux distribution which does not
      require a subscription and has online repositories enabled by default.
